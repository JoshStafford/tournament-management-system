from django.contrib import admin
from . import models
from users import models as user_models

# Register your models here.

admin.site.register(models.global_news)
admin.site.register(user_models.Tournament)
admin.site.register(user_models.Participants)
admin.site.register(user_models.Notifications)