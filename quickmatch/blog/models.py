from django.db import models
# from django.utils import timezone

# Create your models here.

class global_news(models.Model):
	# news_id = models.IntegerField(primary_key=True)
	title = models.CharField(max_length=100)
	date = models.DateField(auto_now=True, editable=False)
	content = models.CharField(max_length=500)

	class Meta:
		verbose_name_plural = "News"

	def __str__(self):
   		return self.title

class tournament(models.Model):
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=500)
	date_time = models.DateTimeField()