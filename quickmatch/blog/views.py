from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import models
from django.contrib.admin.views.decorators import staff_member_required
from . import forms
from django.contrib import messages


from django.shortcuts import render_to_response
# Create your views here.

def home(request):
	news = models.global_news.objects.all()[::-1]
	return render(request, 'blog/home.html', {'news': news, 'title': 'Home'})

def handler404(request, exception, template_name="404.html"):
    response = render_to_response("blog/404.html")
    response.status_code = 404
    return response

def handler500(request, exception, template_name="404.html"):
    response = render_to_response("blog/404.html")
    response.status_code = 500
    return response

@staff_member_required
def update_news(request):
	if request.method == 'POST':
		form = forms.NewsUpdateForm(request.POST)
		if form.is_valid():
			models.global_news(None, form.cleaned_data["title"], None, form.cleaned_data["content"]).save()
			# username = form.cleaned_data.get('username')
			messages.success(request, f'News item created!')
			return redirect('blog-home')

	else:
		form = forms.NewsUpdateForm()

	news = models.global_news.objects.all()[::-1]
	return render(request, 'blog/news-update.html', {'form': form, 'title': 'Create', 'news': news})


