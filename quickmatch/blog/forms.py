from django import forms
from . import models as blog_models


class NewsUpdateForm(forms.Form):
	title = forms.CharField(label=("Post Title"), required=True)
	content = forms.CharField(label=("Post Content"), required=True)

	class Meta:
		model = blog_models.global_news
		fields = ("title", "content")

	# def save(self, commit=True):
	# 	news = super(NewsUpdateForm, self).save(commit=False)
	# 	# user.email = self.cleaned_data["email"]
	# 	if commit:
	# 		news.save()
	# 	return news