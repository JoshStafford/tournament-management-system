"""quickmatch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from users import views as user_views
from blog import views as blog_views
from django.contrib.auth import views as auth_views
from users.views import TournamentListView, TournamentDetailView

handler404 = 'blog_views.handler404'


urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('home/', include('blog.urls')),
    path('register/', user_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('profile/', user_views.profile, name='profile'),
    path('dashboard/', user_views.dashboard, name='dashboard'),
    path('search/', TournamentListView.as_view(), name='search'),
    path('tournament/<slug>/', TournamentDetailView.as_view(), name='tournament-detail'),
    path('create/', user_views.create_tournament, name='create'),
    path('update_news/', blog_views.update_news, name='update-news'),
    path('my_tournaments/', user_views.my_tournaments, name='my-tournaments'),
    # path('login/', user_views.)
    # path('accounts/', include('django.contrib.auth.urls')), # new

    path('', auth_views.LoginView.as_view(template_name='users/login.html')),
    path('users/', user_views.users, name='users'),
    path('news/', user_views.news, name='news'),

]
