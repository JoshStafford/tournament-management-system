# Generated by Django 2.1.7 on 2019-03-14 10:08

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import users.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('users', '0021_participants_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rounds',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('round_no', models.IntegerField()),
                ('player', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Tournament Rounds',
            },
        ),
        migrations.AlterField(
            model_name='tournament',
            name='participant_number',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(100), users.models.validate_even]),
        ),
        migrations.AddField(
            model_name='rounds',
            name='tournament',
            field=models.ForeignKey(editable=False, on_delete=django.db.models.deletion.PROTECT, to='users.Tournament'),
        ),
    ]
