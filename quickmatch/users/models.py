from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.core.exceptions import ValidationError

def validate_even(value):
    if value % 2 != 0:
        raise ValidationError(u'%s is not an even number' % value)


# Create your models here.

class Tournament(models.Model):
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=500)
	# rounds = models.IntegerField()
	date_created = models.DateField(auto_now=True, editable=False)
	participant_number = models.IntegerField(validators=[MaxValueValidator(100), validate_even])
	is_public = models.CharField(max_length=50)
	host = models.ForeignKey(User, on_delete=models.PROTECT)

	class Meta:
		verbose_name_plural = "Tournaments"

	def __str__(self):
   		return self.title


class Participants(models.Model):
	player = models.ForeignKey(User, editable=False, on_delete=models.PROTECT)
	tournament = models.ForeignKey(Tournament, editable=False, on_delete=models.PROTECT)
	active = models.BooleanField()

	class Meta:
		verbose_name_plural = "Participants"

	def __str__(self):
   		return self.player.username


class Notifications(models.Model):
	player = models.ForeignKey(User, editable=False, on_delete=models.PROTECT)
	content = models.CharField(max_length=100)
	date = models.DateField(auto_now=True, editable=False)

	class Meta:
		verbose_name_plural = "Notifications"

	def __str__(self):
   		return self.player.username

class Rounds(models.Model):
	player = models.ForeignKey(User, editable=False, on_delete=models.PROTECT)
	tournament = models.ForeignKey(Tournament, editable=False, on_delete=models.PROTECT)
	round_no = models.IntegerField()

	class Meta:
		verbose_name_plural = "Tournament Rounds"


	def __str__(self):
		return ("%s: %s - round %d" % (self.player.username, self.tournament.title, self.round_no))


class Pairs(models.Model):
        tournament = models.ForeignKey(Tournament, editable=False, on_delete=models.PROTECT)
        player1 = models.ForeignKey(User, editable=True, on_delete=models.PROTECT)
        player2 = models.ForeignKey(User, editabl=True, on_delete=models.PROTECT)
        round_no = models.IntegerField()

        class Meta:
                verbose_name_plural = "Round Pairs"

        def __str__(self):
                return ('%s : %s - %s' % (self.player1.username, self.player2.username, self.tournament.title))
