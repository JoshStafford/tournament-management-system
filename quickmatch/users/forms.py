from django.forms import EmailField, CharField, IntegerField, BooleanField, DateField, DateTimeField

from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from . import models as user_models
from django import forms


class EmailUserCreationForm(UserCreationForm):
	email = EmailField(label=("Email Address"), required=True,
		help_text=_("Required."))

	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2")

	def save(self, commit=True):
		user = super(EmailUserCreationForm, self).save(commit=False)
		user.email = self.cleaned_data["email"]
		if commit:
			user.save()
		return user

class TournamentCreationForm(forms.Form):

	title = CharField(label=("Tournament Title"), required=True)
	description = CharField(label=("Tournament Description"), required=True, 
		help_text=("Give a brief description of your tournament."))
	# date_created = DateTimeField()
	participant_number = IntegerField(label=("Number of participants (max 100)"), required=True)
	is_public = BooleanField(label=("Public tournament?"), required=False)

	class Meta:
		model = user_models.Tournament
		fields = ("title", "description", "participant_number", "is_public")

	# def save(self, commit=True):
	# 	t = super(TournamentCreationForm, self).save(commit=False)
	# 	if commit:
	# 		t.save()
	# 	return t

class JoinTournamentForm(forms.Form):

	class Meta:
		model = user_models.Participants
		fields = ("")

class DeleteTournamentForm(forms.Form):

	class Meta:
		model = user_models.Tournament
		fields = ("")

class DeleteForm(forms.Form):

	class Meta:
		fields = ("")