from django.shortcuts import render, redirect # from django.contrib.auth.forms import UserCreationForm
from . import forms
from django.contrib import messages
from blog import models
from users import models as user_models
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormMixin





def register(request):
	if request.method == 'POST':
		form = forms.EmailUserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			# username = form.cleaned_data.get('username')
			# messages.success(request, f'Account created for {username}!')
			return redirect('login')

	else:
		form = forms.EmailUserCreationForm()
	news = models.global_news.objects.all()[::-1]
	return render(request, 'users/register.html', {'form': form, 'title': 'Register', 'news': news})

# def login(request):
# 	if request.method == 'POST':
# 		form = 

@login_required
def profile(request):
	return render(request, 'users/profile.html', {'title': 'Profile'})

@staff_member_required
def users(request):

	if request.method == 'POST':
		form = forms.DeleteForm(request.POST)
		if form.is_valid():
			for u in User.objects.all():
				if u.username in request.POST:
					user_models.Participants.objects.filter(player=u.id).delete()
					user_models.Tournament.objects.filter(host=u.id).delete()
					user_models.Notifications.objects.filter(player=u.id).delete()
					User.objects.filter(username=u.username).delete()
					break

			# username = form.cleaned_data.get('username')
			# messages.success(request, f'Account created for {username}!')
			return redirect('users')

	else:
		form = forms.DeleteForm()
	

	user_list = User.objects.filter(is_staff=False)
	staff_list = User.objects.filter(is_staff=True)
	news = models.global_news.objects.all()[::-1]
	return render(request, 'users/users.html', {'staff': staff_list, 'users': user_list, 'title': 'User List', 'news': news})

@login_required
def dashboard(request):
	username = None
	if request.user.is_authenticated:
		username = request.user.get_username()

	t = user_models.Participants.objects.filter(player=request.user)
	tournaments = []
	for item in t:
		tournaments.append(user_models.Tournament.objects.get(title = item.tournament.title))

	context = {
		'title': 'Dashboard',
		'news': models.global_news.objects.all()[::-1],
		'Tournaments': tournaments[::-1],
		'notifications': user_models.Notifications.objects.filter(player=request.user)[::-1]
	}

	return render(request, 'users/dashboard.html', context)

# @login_required
def search(request):
	t = user_models.Tournament.objects.filter(is_public=True)
	return render(request, 'users/search.html', {'title': 'Search', 'Tournaments': t})

# @login_required
class TournamentListView(ListView):
	model = user_models.Tournament
	template_name = 'users/search.html'
	context_object_name = 'Tournaments'
	ordering=['-date_created']

	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context
		context = super(TournamentListView, self).get_context_data(**kwargs)
		# Add in a QuerySet of all the books
		context['Tournaments'] = user_models.Tournament.objects.filter(is_public=True)[::-1]
		return context

# @login_required
class TournamentDetailView(FormMixin, DetailView):
	model = user_models.Tournament
	slug_field = 'title'
	form_class = forms.JoinTournamentForm
	context = []

	def get_success_url(self):
		return reverse('dashboard')

	def get_context_data(self, **kwargs):
		context = super(TournamentDetailView, self).get_context_data(**kwargs)
		context['participants'] = user_models.Participants.objects.filter(tournament=context['object'].id)
		context['number'] = len(context['participants'])
		context['rounds'] = user_models.Rounds.objects.filter(tournament=context['object'].id)
		context['form'] = self.get_form()
		return context

	def post(self, *args, **kwargs):
		if self.request.user.is_authenticated:
			self.object = self.get_object()
			form = self.get_form()
			if 'join' in self.request.POST:
				if form.is_valid():
					return self.form_valid_join(form)
				else:
					return self.form_valid_join(form)
			elif 'leave' in self.request.POST:
				if form.is_valid():
					return self.form_valid_leave(form)
				else:
					return self.form_valid_leave(form)

	def form_valid_join(self, form):
		context = self.get_context_data(object=self.object)
		t = user_models.Participants.objects.filter(player=self.request.user.id, tournament=context['object'].id)
		if(len(t) == 0):
			user_models.Notifications(None, context['object'].host.id, ('%s has joined %s') % (self.request.user.username, context['object'].title)).save()
			user_models.Participants(None, self.request.user.id, context['object'].id, True).save()
			return redirect('dashboard')
		else:
			return redirect('search')

	def form_valid_leave(self, form):
		context = self.get_context_data(object=self.object)
		t = user_models.Participants.objects.filter(player=self.request.user.id, tournament=context['object'].id)
		if(len(t) > 0):
			user_models.Notifications(None, context['object'].host.id, ('%s has left %s') % (self.request.user.username, context['object'].title)).save()
			user_models.Participants.objects.filter(player=self.request.user.id, tournament=context['object'].id).delete()
			return redirect('dashboard')
		else:
			return redirect('search')






@login_required
def create_tournament(request):
	if request.method == 'POST':
		form = forms.TournamentCreationForm(request.POST)
		if form.is_valid():
			user_models.Tournament(None, 
									form.cleaned_data["title"], 
									form.cleaned_data["description"], 
									None, 
									form.cleaned_data["participant_number"], 
									form.cleaned_data["is_public"],
									request.user.id).save()


			# username = form.cleaned_data.get('username')
			messages.success(request, f'Tournament successfully created!')
			return redirect('blog-home')

	else:
		form = forms.TournamentCreationForm()

	news = models.global_news.objects.all()[::-1]


	context = {
		'form': form, 
		'title': 'Create', 
		'news': news
	}

	return render(request, 'users/tournament-creation.html', context)

def my_tournaments(request):

	news = models.global_news.objects.all()[::-1]
	hosted = user_models.Tournament.objects.filter(host=request.user.id)[::-1]

	if request.method == 'POST':
		form = forms.DeleteForm(request.POST)
		
		for t in hosted:
			if t.title in request.POST:
				notifyOfDeletion(t)
				user_models.Participants.objects.filter(tournament=t.id).delete()
				user_models.Tournament.objects.get(title=t.title).delete()

		return redirect('my-tournaments')


	else:
		form = forms.DeleteForm()


	context = {
		'news': news,
		'tournaments': hosted,
		'form': form
	}

	return render(request, 'users/my_tournaments.html', context)

def notifyOfDeletion(t):
	players = user_models.Participants.objects.filter(tournament=t.id)

	for p in players:
		user_models.Notifications(None, p.player.id, ('Tournament \'%s\' has been deleted') % t.title, None).save()


@staff_member_required
def news(request):
	if request.method == 'POST':
		form = forms.DeleteForm(request.POST)
		if form.is_valid():
			print('what')
			news = models.global_news.objects.all()

			for item in news:
				if item.title in request.POST:
					item.delete()

		return redirect('news')


	else:
		form = forms.EmailUserCreationForm()
	news = models.global_news.objects.all()[::-1]
	return render(request, 'users/news.html', {'news':news})
